module Hydro

using NCDatasets
using Dates
using PyPlot
using RollingFunctions


struct Debit
    q::Array{N,1} where N
    datevec::Array{N,1} where N
    bassin::String
    history::String
end

function Debit(q; datevec=DateTime(2000,01,01), bassin="NA", history="")
    return Debit(q, datevec, bassin, history)
end

include("functions.jl")

export Debit
export plot
export runmean

end # module
