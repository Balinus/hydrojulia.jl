"""
    plot(D::Debit; lw::Int=2, label::String="", titlestr::String="", linestyle="-", gridfig::Bool=true)

Produit une figure de l'évolution temporelle du débit de la structure D.
"""
function plot(D::Debit; lw::Int=2, label::String="", titlestr::String="", linestyle="-", gridfig::Bool=true)

    timevec_str = string.(D.datevec)
    if length(D.datevec) >= 20
        nb_interval_tmp = length(D.datevec)/8
        nb_int = roundup(nb_interval_tmp, 5)
    else
        nb_int = 1
    end

    if isempty(label)
        label = D.bassin
    end

    figh = PyPlot.plot(1:length(timevec_str), D.q, lw=lw, label=label, linestyle=linestyle)
    PyPlot.xticks(1:nb_int:length(timevec_str), timevec_str[1:nb_int:end], rotation=10)

    if isempty(titlestr)
        titlestr = "Débit"
    end
    title(titlestr)
    if gridfig
        grid("on")
    end

    legend()

    return true, figh

end

"""
    runmean(D::Debit, window::Real)

Retourne un débit filtré.
"""
function runmean(D::Debit, window::Real)

    filtered = runmean(D.q, window)
    history = string(D.history, " running mean of ", window)
    return Debit(filtered, D.datevec, D.bassin, history)

end

function roundup(i,v)
    return round(Int, i/v)*v
end
