### Démo Julia Hydro-Québec - 25 Février 2019

Exemple de module simple pour gérer les débits.


On définit un type "custom" `Debit` qui permet de "stocker" toute l'information reliée au débit et qui peut être ré-utilisée au besoin dans les fonctions subséquentes.

```julia
struct Debit
    q::Array{N,1} where N
    datevec::Array{N,1} where N
    bassin::String
    history::String
end
```

On définit ensuite quelques fonctions sur ces types.

```julia
function plot(D::Debit; lw::Int=2, label::String="", titlestr::String="", linestyle="-", gridfig::Bool=true)

    timevec_str = string.(D.datevec)
    if length(D.datevec) >= 20
        nb_interval_tmp = length(D.datevec)/8
        nb_int = roundup(nb_interval_tmp, 5)
    else
        nb_int = 1
    end

    if isempty(label)
        label = D.bassin
    end

    figh = PyPlot.plot(1:length(timevec_str), D.q, lw=lw, label=label, linestyle=linestyle)
    PyPlot.xticks(1:nb_int:length(timevec_str), timevec_str[1:nb_int:end], rotation=10)

    if isempty(titlestr)
        titlestr = "Débit"
    end
    title(titlestr)
    if gridfig
        grid("on")
    end

    legend()

    return true, figh

end
```

Fonction qui retourne une moyenne mobile. Le type de retour est aussi de type `Debit`

```julia
function mvavg(D::Debit, window::Real)

    filtered = runmean(D.q, window)
    history = string(D.history, " running mean of ", window)
    return Debit(filtered, D.datevec, D.bassin, history)
end
```
